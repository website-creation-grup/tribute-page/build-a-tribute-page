# Ayrton Senna Tribute Page

This page is dedicated to the legendary Formula 1 driver Ayrton Senna. It includes:

- Driver Information: A brief overview of Ayrton Senna's life, career, and impact on the world of motorsports.
- Wikipedia Reference: At the end of the page, there’s a link to the full Wikipedia article for those interested in exploring more about Senna's life and achievements.

A small tribute to one of the greatest drivers in Formula 1 history!